# I. Hướng dẫn deploy FE
### Bước 1: Chuẩn bị source code FE và đăng nhập vào Cloudflare:
1. Chọn **Workers&Page**
2. Chọn **Create Application**
![Bước 1](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694065832/deploy/z4671262600221_e537fa83357e16883ea270da9c76a2fa_tkrxuh.jpg)
### Bước 2: Chọn **Page** và kết nối với Git
![Bước 2](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694066013/deploy/b71183b9-1e46-47ef-80fa-7a4117ef7a94_l5tofz.png)
## Bước 3: Bạn chọn một trong 2 lựa chọn để kết nối với Github hoặc GitLab (Hình 1). Và nếu đã kết nối trước đó thì sẽ hiển thị ở Hình 2.
Hình 1
![Hình 1](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694066111/deploy/z4671272833642_75b7ecd859056db1185bb0c46460960d_sqs4y9.jpg)

Hình 2
![Mô tả hình ảnh](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694066205/deploy/d2f002a2-6d47-4db1-9290-42b5a16aec38_qojfal.png)
### Bước 4: Chọn dự án FE mà bạn muốn deploy
### Bước 5: Kiểm tra lại thông tin, **SAVE AND DEPLOY**
1. Kiểm tra đúng dự án chưa
2. Kiểm tra nhánh chứa source dự án
3. Kiểm tra và chọn đúng framework dự án (ở đây đang là Create React App)
4. Câu lệnh build 
5. thư mục chứa
6. Save và deploy
![Deploy](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694066447/deploy/766e4a86-bffc-4aed-85f7-010dc46230a6_p3ld9u.png)


# Hướng dẫn quản lý Website:
### link Website : https://learningvietnamworks.pages.dev/
### Quản lý:
1. Với giảng Viên:
![giảng viên](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694066775/deploy/6e91b288-c679-4a2d-9203-8281685c5c85_vrwvqn.png)

- Cột lecturerName: Nhập tên của giảng Viên
- Cột LecturerDescription: Nhập thông tin của giảng viên (Lưu ý nếu giảng viên có quá nhiều thông tin, vui lòng phân tách từng thông tin thành từng đoạn ngắn)
- Cột img: là Link chứa hình ảnh của Giảng Viên
-> Có thể thêm mới một giảng viên, không được xóa
2. Với User:
![người dùng](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694067223/deploy/66690b42-41ba-4a4b-8f5b-fcb2356ceeee_ld06rs.png)

- Chứa tên đăng nhập, mật khẩu (đã mã hóa), email, số điện thoại và họ tên khách hàng/ người dùng.
- Vui lòng không sửa chữa, thay đổi bất kỳ thông tin vì đây là thông tin của khách hàng dùng để đăng nhập Website
3. Với Order: 
![Order](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694067333/deploy/z4671316620098_647ded062d1e158fa3ae8ef7d54ba59c_i3pafv.png)
- Chứa thông tin đơn hàng của khách hàng
- Chỉ được xem thông tin. Vui lòng không được chỉnh sửa
4. Với Courses:
![Khóa học](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694067340/deploy/z4671318312543_50e344981d974108dc82096418687226_yhlxn6.jpg)
- Chứa các thông tin : tên khóa học, giảng viên (liên kết với bảng Lecturers), hình ảnh (link chứa hình ảnh khóa học), giá khóa học, thể loại (có thể chọn 1 hoặc nhiều thể loại cho khóa học), thông tin khóa học, lợi ích khóa học, tại sao tham gia khóa học, yêu cầu khóa học và một số thông tin quảng bá khóa học
- Có thể thêm hoặc Xóa Khóa học.
5. Với Lession:
![Phần học](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694067843/deploy/z4671337648607_767c47e6e95f8b5d30b6254d399cc2b2_eiobjf.jpg)
- Mô tả các phần học của một khóa học và liên kết với bảng Courses.
- Có thể thêm hoặc xóa. Lưu ý mỗi phần học chỉ được liên kết với một Khóa học, một khóa học thì có nhiều phần học.
6. Với Topic:
![Bài học](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694067860/deploy/b5a76fb1-affb-43e2-b9aa-44cd4eef1d26_pk7cwy.png)
- Tương tự Lession, Topic mô tả các bài học của một phần học.
- Có thể thêm hoặc xóa. Nhưng một Topic chỉ được liên kết với một Lession, và một lession sẽ có nhiều topic
